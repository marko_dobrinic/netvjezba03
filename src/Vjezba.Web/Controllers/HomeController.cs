﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace Vjezba.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Pogledati zadatak 3.1
        /// Ova akcija se poziva prilikom poziva: GET zahtjev na /Home/Contact
        /// Ova akcija kao rezultat vraća inicijalnu (praznu) formu za popuniti podatke
        /// Formu je potrebno kreirati kao dio zadatka
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Jednostavan način proslijeđivanja poruke iz Controller -> View.";

            //Kao rezultat se pogled /Views/Home/Contact.cshtml renderira u "pravi" HTML
            //Primjetiti - View() je poziv funkcije koja uzima cshtml template i pretvara ga u HTML
            //Zasto bas Contact.cshtml? Jer se akcija zove Contact, te prema konvenciji se "po defaultu" uzima cshtml datoteka u folderu Views/CONTROLLER_NAME/AKCIJA.cshtml

            return View();
        }

        /// <summary>
        /// Ova akcija se poziva kada na formi za kontakt kliknemo "Submit"
        /// URL ove akcije je /Home/SubmitQuery, uz POST zahtjev isključivo - ne može se napraviti GET zahtjev zbog [HttpPost] parametra
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitQuery(FormCollection formData)
        {
                  //Ovdje je potrebno obraditi podatke i pospremiti finalni string u ViewBag                 
                  string user_name = formData["name"];
                  string user_surname = formData["surname"];
                  string email = formData["email"];
                  string message = formData["message_area"];
                  string selection = formData["message_selection"].ToString(); 
                  string newsletter = formData["newsletter"].ToString();

                  Debug.WriteLine("..............." + newsletter);
                  string obavijest = "";

                  if (newsletter == "false")
                  {
                        obavijest = " nećemo vas obavijestiti ";
                  }
                  else
                  {
                        obavijest = " obavijestit ćemo vas ";
                  }      


                  //Kao rezultat se pogled /Views/Home/ContactSuccess.cshtml renderira u "pravi" HTML
                  //Kao parametar se predaje naziv cshtml datoteke koju treba obraditi (ne koristi se default vrijednost)
                  //Trazenu cshtml datoteku je potrebno samostalno dodati u projekt

                  string result = "Dragi " + user_name + " " 
                        + user_surname + "(" + email + ") "
                        + "zaprimili smo vašu poruku  te će vam se netko ubrzo javiti. "
                        + "Sadržaj vaše poruke je : "
                        + "[" + selection + "]"
                        + " " + message + ". "
                        + "Također, " + obavijest
                        + " o daljnjim promjenama preko newslettera.";                    
                        

                  ViewBag.message = result;
                  return View("ContactSuccess");
        }


    }
}